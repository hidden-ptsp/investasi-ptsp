import 'bootstrap'

import mapboxgl from 'mapbox-gl'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'
import * as turf from '@turf/turf'
import MapboxDraw from '@mapbox/mapbox-gl-draw'
import 'select2'
import ApexCharts from 'apexcharts'

import '../scss/index.scss'

var urlGeoJson = 'http://103.214.112.123:3000/';
mapboxgl.accessToken = 'pk.eyJ1IjoibWVudGhvZWxzciIsImEiOiJja3M0MDZiMHMwZW83MnVwaDZ6Z2NhY2JxIn0.vQFxEZsM7Vvr-PX3FMOGiQ';
const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/menthoelsr/ckp4wrapq11m117pf2lr49l5t'
});


var geocoder = new MapboxGeocoder({
  accessToken: mapboxgl.accessToken,
  countries: 'id',
  // types: 'district',
  placeholder: "Cari kelurahan disini...",
  marker: false,
  flyTo: {
    easing: function (t) {
      return t;
    }
  },
  render: function (item) {
    var maki = item.properties.maki || 'marker';
    return (
      "<div class='geocoder-dropdown-item'><img class='geocoder-dropdown-icon mr-3' src='https://unpkg.com/@mapbox/maki@6.1.0/icons/" +
      maki +
      "-15.svg'><span class='geocoder-dropdown-text' style='line-height:2;'>" +
      item.text +
      '</span></div>'
    );
  },
  getItemValue: function (item) {
    addSourceLayer(item.text)


    var maki = item.place_name || 'marker';
    return maki
  },
  mapboxgl: mapboxgl
});
geocoder.on('clear', function () {
  // results.innerText = '';
});

map.on('style.load', function () {
  map.on('click', function (e) {
    const coornya = e.lngLat;
    var lats = coornya.lat.toString()
    var lngs = coornya.lng.toString()
    lats = lats.slice(0, -7)
    lngs = lngs.slice(0, -7)
    setTimeout(() => {
      $('#forCoordinates').html(`
        <th>Koordinat</th>
        <td><a class="font-weight-bold" href="https://www.google.com/maps/search/%09${lats},${lngs}" target="_blank">${lats}, ${lngs}</a></td>
      `)
    }, 500);
  });

  // Marker onclick
  var marker = new mapboxgl.Marker({
    color: '#f0932b',
    scale: 0.7
  });

  function add_marker(event) {
    var coordinates = event.lngLat;
    marker.setLngLat(coordinates).addTo(map);
  }
  map.on('click', add_marker);


  map.addSource('wilayahindex', {
    'type': 'geojson',
    'data': 'http://103.214.112.123:3000/choro'
  });

  map.addLayer({
    'id': 'wilayahindex_fill',
    'type': 'fill',
    'source': 'wilayahindex',
    'paint': {
      'fill-color': [
        "interpolate",
        ["linear"],
        ["get", "Total omzet"],
        0,
        "#ffeda0",
        5000000000,
        "#ffe675",
        9000000000,
        "#ffdf52",
        13000000000,
        "#ffd61f",
        17000000000,
        "#e0b700",
        20396854609,
        "#caa502"
      ],
      'fill-opacity': 0.7,
      'fill-outline-color': 'red'
    },
    'layout': {
      'visibility': 'visible'
    }
  });
})

map.on('touchend', 'points', function (e) {
  console.log('aa')
});
map.on('touchstart', 'points', function (e) {
  console.log('aa')
});

// DRAW POLYGON

  var $window = $(window);

  function checkWidth() {
      var windowsize = $window.width();
      if (windowsize < 768) {
        // map.removeControl(draw)
      }else {
        const draw = new MapboxDraw({
          displayControlsDefault: false,
          controls: {
            polygon: true,
            trash: true
          }
        });

        map.addControl(draw)

        map.on('draw.create', updateArea);
        map.on('draw.delete', updateArea);
        map.on('draw.update', updateArea);

        function updateArea(e) {
          const data = draw.getAll();
          const coordinates = data.features[0].geometry.coordinates[0]
          if (data.features.length > 0) {
            console.log('data_lat_lng', JSON.stringify(coordinates))
          } else {
            console.log('del', data)
            if (e.type !== 'draw.delete')
              console.log(e)
          }
        }
      }
  }
  checkWidth();
// DRAW POLYGON

var layerList = document.getElementById('menu');
var inputs = layerList.getElementsByTagName('input');

for (var i = 0; i < inputs.length; i++) {
  inputs[i].onclick = switchLayer;
}

map.addControl(geocoder);
geocoder.addTo('#cari_wilayah');
map.addControl(new mapboxgl.NavigationControl());

// FILL Action Click
var setAttrClick
map.on('click', 'wilayah_fill', function (e) {
  var dt = e.features[0].properties;
  setAttrClick = e
  $('#forDataLayer').html('')
  getRadius(e)
  getPersilBPN(e)
  const larea = dt['luas-area'] / 10000

  var optionsCol = {
    series: [{
      name: 'Jumlah',
      data: [dt.U1, dt.U2, dt.U3, dt.U4, dt.U5]
    }],
    chart: {
      type: 'bar',
      height: 180,
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '55%',
        endingShape: 'rounded'
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    xaxis: {
      categories: ['20-29', '30-39', '40-49', '50-59', '60-69'],
      title: {
        text: 'Usia'
      }
    },
    yaxis: {
      title: {
        text: 'Jumlah'
      }
    },
    fill: {
      opacity: 1
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return val + ' Orang'
        }
      }
    },
    legend: {
      show: false
    }
  }

  var optionsPie = {
    chart: {
      type: 'pie'
    },
    series: [dt.Jasa, dt.Perdagangan, dt.Produksi],
    labels: ['Jasa', 'Perdagangan', 'Produksi'],
    legend: {
      show: false
    },
    dataLabels: {
      offset: 0,
      minAngleToShowLabel: 10,
      formatter: function (val, opts) {
        return opts.w.config.series[opts.seriesIndex]
      }
    }
  }

  var htmlPopupLayer = `
              <tr>
                <th>Daerah</th>
                <td>${dt.Kelurahan}, ${dt.Kecamatan}, ${dt.Kota}</td>
              </tr>
              <tr id="forCoordinates">
              </tr>
              <tr>
                <th>Luas Area</th>
                <td>${larea.toFixed(2)} ha</td>
              </tr>
              <tr>
              <td class="font-weight-bold text-center" colspan="3">Usaha Mikro Kecil</td>
            </tr>
              <tr>
                <td colspan="2">
                  <table class="w-100 table mb-0">
                    <tbody>
                      <tr>
                        <th>Pelaku Usaha :</th>
                        <td>${dt.Jumlah} Orang</td>
                        <th>Total Omzet :</th>
                        <td>Rp ${separatorNum(dt['Total omzet'])} /bulan</td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          <div class="row">
                            <div class="col-md-5 col-sm-12" style="margin:auto;">
                              <div id="pieChart"></div>
                            </div>
                            <div class="col-md-7 col-sm-12" style="margin:auto;">
                              <div id="colChart"></div>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
              <td class="font-weight-bold text-center" colspan="3">Pendapatan Rata-Rata</td>
            </tr>
            <tr>
              <td colspan="2">
              <table class="w-100 table mb-0">
                <tbody>
                  <tr>
                    <th>0-5 Jt :</th>
                    <td>${separatorNum(dt.R1)} orang</td>
                    <th>6-10 Jt :</th>
                    <td>${separatorNum(dt.R2)} orang</td>
                  </tr>
                  <tr>
                    <th>11-15 Jt :</th>
                    <td>${separatorNum(dt.R3)} orang</td>
                    <th>16-20 Jt :</th>
                    <td>${separatorNum(dt.R4)} orang</td>
                  </tr>
                  <tr>
                    <th>> 20 Jt :</th>
                    <td>${separatorNum(dt.R5)} orang</td>
                    <th>N/A :</th>
                    <td>${separatorNum(dt.R6)} orang</td>
                  </tr>
                </tbody>
              </table>
              </td>
            </tr>
`
  /* <tr>
  <th>Kepadatan Penduduk</th>
  <td>${separatorNum(dt.Kepadatan)} /km&sup2;</td>
  </tr> */

  // modal
  $('#forDataLayer').append(htmlPopupLayer)

  var chart1 = new ApexCharts(document.querySelector("#pieChart"), optionsPie)
  var chart2 = new ApexCharts(document.querySelector("#colChart"), optionsCol)

  chart1.render()
  chart2.render()
});
map.on('mouseenter', 'wilayah_fill', function () {
  map.getCanvas().style.cursor = 'default';
});
map.on('mouseleave', 'wilayah_fill', function () {
  map.getCanvas().style.cursor = 'default';
});

map.on('click', 'eksisting_fill', function (e) {
  var dt = e.features[0].properties;
  var htmlPopupLayer = `<tr>
                      <th>Lahan Eksisting</th>
                      <td>${dt.Kegiatan}</td>
                    </tr>`

  // modal
  $('#forDataLayer').append(htmlPopupLayer)
});
map.on('mouseenter', 'eksisting_fill', function () {
  map.getCanvas().style.cursor = 'default';
});
map.on('mouseleave', 'eksisting_fill', function () {
  map.getCanvas().style.cursor = 'default';
});

// map.on('click', 'bpn_fill', function (e) {
//   var dt = e.features[0].properties;
//   var htmlPopupLayer = `<tr>
//               <td class="font-weight-bold text-center" colspan="4">Badan Pertanahan Nasional</td>
//             </tr>
//             <tr>
//               <td colspan="2">
//               <table class="w-100 table mb-0">
//                 <tbody>
//                   <tr>
//                     <th>Tipe Hak :</th>
//                     <td>${dt['Tipe Hak']}</td>
//                     <th>Luas :</th>
//                     <td>${separatorNum(dt.Luas)} m&sup2;</td>
//                   </tr>
//                 </tbody>
//               </table>
//               </td>
//             </tr>`

//   // modal
//   $('#forDataLayer').append(htmlPopupLayer)
// });

map.on('click', 'njop_fill', function (e) {
  var dt = e.features[0].properties;
  var htmlPopupLayer = `<tr>
              <th>Perkiraan Harga</th>
              <td>Rp ${separatorNum(dt.NJOP_MIN)},-  s/d  Rp ${separatorNum(dt.NJOP_MAX)},- /m&sup2;</td>
            </tr>`

  // modal
  $('#forDataLayer').append(htmlPopupLayer)
});
map.on('mouseenter', 'njop_fill', function () {
  map.getCanvas().style.cursor = 'default';
});
map.on('mouseleave', 'njop_fill', function () {
  map.getCanvas().style.cursor = 'default';
});

map.on('click', 'iumk_fill', function (e) {
  var dt = e.features[0].properties;

  var htmlPopupLayer = `<tr>
              <th>Nama Usaha</th>
              <td>${dt['Nama Usaha']}</td>
            </tr>`
  // modal
  $('#forDataLayer').append(htmlPopupLayer)
});

map.on('mouseenter', 'iumk_fill', (e) => {
  map.getCanvas().style.cursor = 'pointer';
  const coordinates = e.features[0].geometry.coordinates.slice();
  const dt = e.features[0].properties
  const nosk = dt['Nomor SK']
  const splitsk = nosk.split('/')
  const noskfix = splitsk[0] + '/**/*****************/*/*****/**/' + splitsk[6]
  const content = `<div class="card">
  <div class="imgcard-container">
    <img src="#" class="card-img-top" id="imgCardIUMK" alt="${dt['Nama Usaha']}" style="height: 160px;object-fit: cover;display:none;">
  </div>
  <div class="card-body p-2">
    <h6 class="mt-0 mb-2 card-title border-bottom">${dt['Nama Usaha']}</h6>
    <div style="line-height: 1.2;">
      <span class="d-block"><b>Pemilik Usaha</b> : ${dt['Pemilik Usaha']}</span>
      <span class="d-block"><b>No. SK</b> : ${noskfix}</span>
      <span class="d-block"><b>Jenis Usaha</b> : ${dt['Jenis Usaha']}</span>
      <span class="d-block"><b>Tenaga Kerja</b> : ${dt['Tenaga Kerja']} Orang</span></div>
    </div>
  </div>`

  while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360
  }
  popup.setLngLat(coordinates).setHTML(content).addTo(map)
  delay(function () {
    getIumk(e)
  }, 500)
});

map.on('mouseleave', 'iumk_fill', () => {
  map.getCanvas().style.cursor = '';
  popup.remove();
});
map.on('click', 'zoning_fill', function (e) {
  // $('#kbliOne').show()
  $('#modalKBLIOne').modal('show')
  var dt = e.features[0].properties;
  var htmlPopupLayer = `<tr>
            <th>Zona</th>
            <td>${dt.Zona}</td>
            </tr>
            <tr>
              <th>Sub Zona</th>
              <td>${dt['Sub-Zona']}
                <div class="subZonaVal badge badge-info d-none">${dt['Sub-Zona']}</div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
              <table class="w-100 table mb-0">
                <tbody>
                  <tr>
                    <th>KDH :</th>
                    <td>${dt.KDH}</td>
                    <th>KDB :</th>
                    <td>${dt.KDB}</td>
                    <th>KLB :</th>
                    <td>${dt.KLB}</td>
                  </tr>
                </tbody>
              </table>
              </td>
            </tr>`

  saveSubZona = dt['Sub-Zona']
  // modal
  $('#forDataLayer').append(htmlPopupLayer)
});
map.on('mouseenter', 'zoning_fill', function () {
  map.getCanvas().style.cursor = 'default';
});
map.on('mouseleave', 'zoning_fill', function () {
  map.getCanvas().style.cursor = 'default';
});

const popup = new mapboxgl.Popup({
  closeButton: false,
  closeOnClick: false
});

var actQ = ''
var saveSubZona = ''
map.on('load', function () {
  getSektorKBLI()

  const layers = [
    '0-4M',
    '5M-8M',
    '9M-12M',
    '13M-16M',
    '17M-20M',
    '> 20M'
  ];
  const colors = [
    '#ffeda0',
    '#ffe675',
    '#ffdf52',
    '#ffd61f',
    '#e0b700',
    '#caa502'
  ];

  // create legend
  const legend = document.getElementById('legends');

  layers.forEach((layer, i) => {
    const color = colors[i];
    const item = document.createElement('div');
    const key = document.createElement('span');
    key.className = 'legend-key';
    key.style.backgroundColor = color;

    const value = document.createElement('span');
    value.innerHTML = `${layer}`;
    item.appendChild(key);
    item.appendChild(value);
    legend.appendChild(item);
  });

  map.on('mousemove', ({
    point
  }) => {
    const states = map.queryRenderedFeatures(point, {
      layers: ['wilayahindex_fill']
    });
    document.getElementById('pd').innerHTML = states.length ?
      `<div>Kelurahan : ${states[0].properties.Kelurahan}</div><p class="mb-0"><strong><em>Rp ${separatorNum(states[0].properties['Total omzet'])}</strong></em></p>` :
      `<p class="mb-0">Arahkan kursor untuk melihat data</p>`;
  });

  map.on('dblclick', (e) => {
    const states = map.queryRenderedFeatures(e.point, {
      layers: ['wilayahindex_fill']
    });
    map.flyTo({
      center: e.lngLat
    });
    actQ = geocoder.query(states[0].properties.Kelurahan)
    addSourceLayer(actQ.inputString)
  });

  map.on('click', (e) => {
    // map.flyTo({
    //   center: e.lngLat,
    // zoom: 14
    // });
  });

});

// Other JS
$("#radiusVal").html(separatorNum($('#radiusRange').val() / 1000) + ' km');
$("#radiusRange").change(function () {
  $("#radiusVal").html(separatorNum($(this).val() / 1000) + ' km');
  if ($('.infoLokasi').is(':visible')) {
    getRadius(setAttrClick)
  }
});

// FUNCTION FUNGSI
function addSourceLayer(item) {
  var api = ["wilayah", "zoning", "njop", "eksisting", "iumk"]

  for (var i = 0; i < api.length; i++) {
    const dt = api[i]
    if (map.getLayer(dt + '_fill')) {
      map.removeLayer(dt + '_fill')
      $('#menus').html('')
      $('.lblLayer').hide()
      $('.closeCollapse').hide()
      $('#radiusSlide').hide()
    }
    if (map.getLayer(dt + '_dot')) {
      map.removeLayer(dt + '_dot')
      $('#menus').html('')
      $('.lblLayer').hide()
      $('.closeCollapse').hide()
      $('#radiusSlide').hide()
    }
    if (map.getSource(dt)) {
      map.removeSource(dt)
      $('#menus').html('')
      $('.lblLayer').hide()
      $('.closeCollapse').hide()
      $('#radiusSlide').hide()
    }

    map.addSource(dt, {
      'type': 'geojson',
      'data': urlGeoJson + dt + '/' + item
    });
  }

  addLayers()
  $('.lblLayer').show()
  $('.closeCollapse').show()
  // $('#kbliTwo').show()
  // $('').show()
  onOffLayers()
}

function addLayers() {
  map.addLayer({
    'id': 'wilayah_fill',
    'type': 'fill',
    'source': 'wilayah',
    'paint': {
      'fill-color': '#00FFFF',
      'fill-opacity': 0.1,
      'fill-outline-color': 'red'
    },
    'layout': {
      'visibility': 'visible'
    }
  });
  map.addLayer({
    'id': 'njop_fill',
    'type': 'fill',
    'source': 'njop',
    'paint': {
      'fill-color': ['get', 'marker-color'],
      'fill-opacity': 0.01
    },
    'layout': {
      'visibility': 'visible'
    },
  });
  map.addLayer({
    'id': 'zoning_fill',
    'type': 'fill',
    'source': 'zoning',
    'paint': {
      'fill-color': ['get', 'fill'],
      'fill-opacity': 1
    },
    'layout': {
      'visibility': 'visible'
    },
  });
  // map.addLayer({
  //   'id': 'bpn_fill',
  //   'type': 'fill',
  //   'source': 'bpn',
  //   'paint': {
  //     // 'fill-color': ['get', 'color'],
  //     'fill-opacity': 0.3
  //   },
  //   'layout': {
  //     'visibility': 'none'
  //   },
  // });
  map.addLayer({
    'id': 'eksisting_fill',
    'type': 'fill',
    'source': 'eksisting',
    'paint': {
      // 'fill-color': ['get', 'color'],
      'fill-opacity': 0.01
    },
    'layout': {
      'visibility': 'visible'
    },
  });
  map.addLayer({
    'id': 'iumk_fill',
    'type': 'circle',
    'source': 'iumk',
    'paint': {
      'circle-color': '#4264fb',
      'circle-radius': 4,
      'circle-opacity': 0.8
    },
    'layout': {
      'visibility': 'visible'
    },
  });
}

function onOffLayers() {
  var toggleableLayerIds = ["wilayah_fill", "wilayahindex_fill", "zoning_fill", "njop_fill", "eksisting_fill", "iumk_fill"];
  var textCnt = ["Wilayah", "Choropleth", "Rencana Kota", "Interpolasi NJOP", "Penggunaan Lahan Eksisting", "Usaha Mikro Kecil"];

  for (var i = 0; i < toggleableLayerIds.length; i++) {
    var id = toggleableLayerIds[i];
    if (!document.getElementById(id)) {


      var div = document.createElement('div')
      div.className = 'form-check ' + id

      var link = document.createElement('input');
      link.id = id;
      link.name = id;
      if (textCnt[i] == 'Wilayah') {
        link.setAttribute("disabled", true)
      }
      link.className = 'form-check-input mt-1';
      link.type = 'checkbox';
      // link.value = id
      div.append(link)

      var label = document.createElement('label')
      label.htmlFor = id
      label.className = 'form-check-label'
      label.innerHTML = textCnt[i]
      div.appendChild(label)

      var mapLayer = map.getLayer(id);

      if (mapLayer.visibility == 'visible') {
        link.checked = true
      } else {
        link.checked = false
      }

      link.addEventListener('change', function (e) {
        var clickedLayer = this.id;
        e.preventDefault();
        e.stopPropagation();
        var visibility = map.getLayoutProperty(clickedLayer, 'visibility');
        if (visibility == 'visible') {
          hideLayer(clickedLayer)
        } else if (visibility == 'none') {
          showLayer(clickedLayer)
        } else {
          $(this).prop('checked', false)
          alert('nda ada')
        }
      });

      $('#menus').append(div)
      $('#menus').show()
      $('#radiusSlide').show()
    }
  }
}

function showLayer(layer) {
  map.setLayoutProperty(layer, 'visibility', 'visible');
}

function hideLayer(layer) {
  map.setLayoutProperty(layer, 'visibility', 'none');
}

function switchLayer(layer) {
  var layerId = layer.target.id;
  map.setStyle('mapbox://styles/menthoelsr/' + layerId);
}

function getRadius(e) {

  var tblRadData = ''
  var getRadVal = $('#radiusRange').val()
  $.ajax({
    url: `http://103.214.112.123:3000/lon/${e.lngLat.lng}/lat/${e.lngLat.lat}/rad/${getRadVal}`,
    method: 'get',
    contentType: false,
    processData: false,
    cache: false,
    beforeSend: function () {
      $('#forDataRad').html('')
    },
    success: function (dt) {
      var cat = [];
      // var getrad = [];
      var dtResp = JSON.parse(dt)
      for (var i in dtResp.features) {
        const dtpar = dtResp.features[i];
        const props = dtpar.properties;
        const geo = dtpar.geometry;
        cat.push({
          name: props.Kategori,
          fasilitas: props.Name,
          jarak: geo.Distance
        })
      }

      function groupBy(collection, property) {
        var i = 0,
          val, index,
          values = [],
          result = [];
        for (var i = 0; i < collection.length; i++) {
          val = collection[i][property];
          index = values.indexOf(val);
          if (index > -1)
            result[index].push(collection[i]);
          else {
            values.push(val);
            result.push([collection[i]]);
          }
        }
        return result;
      }

      var obj = groupBy(cat, "name");
      var leftTab = ''
      var tabFasilitas = ''
      for (var as in obj) {
        const dt = obj[as]
        var cls = ''
        var clst = ''
        if (as == 0) {
          cls = 'active'
          clst = 'show active'
        }

        leftTab += `<a class="nav-link linknav-cust ${cls}" id="${as}-tab" data-toggle="pill" href="#fas${as}" role="tab" aria-controls="${as}" aria-selected="true">${dt[0].name}</a>`

        tabFasilitas = `
      <div class="tab-pane fade ${clst}" id="fas${as}" role="tabpanel" aria-labelledby="${as}-tab">
        <ul class="list-group list-group-flush">

        </ul>
      </div>`
        $('#tabListFasilitas').append(tabFasilitas)
        for (var az in dt) {
          const dta = dt[az]
          // var from = turf.point([e.lngLat.lng, e.lngLat.lat])
          // var to = turf.point(dta.coordinates[0])
          // var options = {
          //   units: 'kilometers'
          // }
          // var distance = turf.distance(from, to, options)
          var distance = dta.jarak
          const linya = `<li class="list-group-item listgroup-cust d-flex justify-content-between align-items-center">${dta.fasilitas}
            <span class="badge badge-primary badge-pill">${Math.round(distance) / 1000} km</span>
          </li>`
          $('#fas' + as + ' ul').append(linya)
        }
      }

      $('#v-pills-tab').html(leftTab)
    },
    error: function (error) {
      console.log(error)
    }

  });
  var htmlPopupRad = `
              <div class="row">
              <div class="col-12">
              <div class="text-center font-weight-bold">- Fasilitas Dalam Radius ${separatorNum($('#radiusRange').val() / 1000)} km -</div>
              </div>
              <div class="col-3 pl-0">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                  
                </div>
              </div>
              <div class="col-9 pl-0 pr-0">
              <div class="tab-content" id="tabListFasilitas">
              </div>
              </div>
            `

  $('#dtRadiusBot').html(htmlPopupRad)
  $('.infoLokasi').show();
}

function getPersilBPN(e) {
  $('#dtBpnBot').html('')
  var htmlPopupLayer = ''
  $.ajax({
    url: `http://103.214.112.123:3000/bpn/${e.lngLat.lng}/${e.lngLat.lat}`,
    method: 'get',
    contentType: false,
    processData: false,
    cache: false,
    beforeSend: function () {},
    success: function (dt) {
      const dtResp = JSON.parse(dt)
      if (dtResp.features != null) {
        const prop = dtResp.features[0].properties
        htmlPopupLayer += `<div class="table-responsive"><table class="table table-striped table-sm mb-1">
          <tbody>
            <tr>
              <td class="font-weight-bold text-center" colspan="4">Badan Pertanahan Nasional</td>
            </tr>
            <tr>
              <td colspan="2">
              <table class="w-100 table mb-0">
                <tbody>
                  <tr>
                    <th>Tipe Hak :</th>
                    <td>${prop.Tipe}</td>
                    <th>Luas :</th>
                    <td>${separatorNum(prop.Luas)} m&sup2;</td>
                  </tr>
                </tbody>
              </table>
              </td>
            </tr>
          </tbody>
        </table></div>`
      }

      $('#dtBpnBot').html(htmlPopupLayer)
    },
    error: function (error) {
      console.log(error)
    }
  })
}

function getIumk(e) {
  const dt = e.features[0].properties
  $.ajax({
    url: `https://iumk.perizinan-dev.com/api/getWithKoordinat`,
    method: 'post',
    headers: {
      'AUTHCODE': '9ee2f95d62b9f67f58ec288b1599cf9c',
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: {
      lat: dt.Lat,
      lng: dt.Long
    },
    beforeSend: function () {},
    success: function (dt) {
      const dtResp = JSON.parse(dt)
      setTimeout(() => {
        // const img = document.getElementById('imgCardIUMK')
        // img.src = dtResp.data[0].file_foto_usaha
        addImageIumk(dtResp.data[0].file_foto_usaha, '#imgCardIUMK')
      }, 500);
    },
    error: function (error) {
      console.log(error)
    }
  })
}

$(function () {
  var Accordion = function (el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {
      el: this.el,
      multiple: this.multiple
    }, this.dropdown)
  }

  Accordion.prototype.dropdown = function (e) {
    var $el = e.data.el,
      $this = $(this),
      $next = $this.next();

    $next.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
  }

  var accordion = new Accordion($('#accordion'), false);
});

$('.close-info').on('click', function (event) {
  resetModal()
  $('.infoLokasi').hide()
})

function resetModal() {
  $('#forDataLayer').html('')
  $('#dtRadiusBot').html('')
}

function getSektorKBLI() {
  $.ajax({
    url: `http://103.214.112.123:3000/kbli/`,
    dataType: 'json',
    beforeSend: function () {},
    success: function (dt) {
      const dtResp = dt.features[0].properties
      var optHtml = ''
      for (var i in dtResp) {
        const fdt = dtResp[i]
        optHtml += `<option value="${fdt.Sektor}">${fdt.Sektor}</option>`
      }
      $('._sektor').html(optHtml)
    },
    error: function (error) {
      console.log(error)
    }
  })
}

function getDataKBLI() {
  console.log(saveSubZona)
  const subzona = saveSubZona
  const sektor = $('#_sektor').val()
  const kegiatan = $('#_kegiatan').val()
  const lokasi = $('#_kegiatanruang').val()
  const skala = $('#_valmodal').val()

  if (subzona != '' && sektor != '' && kegiatan != '' && lokasi != '' && skala != '') {
    $.ajax({
      url: `http://103.214.112.123:3000/kblic/${subzona}/${kegiatan}/${lokasi}/${skala}/${sektor}`,
      dataType: 'json',
      beforeSend: function () {},
      success: function (res) {
        var tblSel = ''
        const dt = res.features[0].properties
        for (var x in dt) {
          const kode = dt[x]['Kode KBLI']
          const kegiatan = dt[x]['Kegiatan']
          const status = dt[x]['Status']
          tblSel += `<tr>
            <td><a href="https://oss.go.id/informasi/kbli-kode?kode=G&kbli=${kode}" target="_blank">${kode}</a></td>
            <td>${kegiatan}</td>
            <td>${status}</td>
          </tr>`
        }

        $('#dtKBLI').html(tblSel)
      },
      error: function (error) {
        console.log(error)
      }
    })
  }
}

function handleImgError() {
  $('#imgCardIUMK').hide()
}

function addImageIumk(imgSource, destination) {
  var img = $('#imgCardIUMK').on('error', handleImgError).attr('src', imgSource)
  $(destination).append(img)
  $(destination).show()
}

class SideNav {
  constructor() {
    this.sideNavEl = document.querySelector('.js-side-nav');
    this.sideNavContainerEl = document.querySelector('.js-side-nav-container');
    this.showButtonEl = document.querySelector('.js-menu-open');
    this.closeButtonEl = document.querySelector('.js-menu-close');

    this.openSideNav = this.openSideNav.bind(this);
    this.closeSideNav = this.closeSideNav.bind(this);
    this.blockClicks = this.blockClicks.bind(this);
    this.onTransitionEnd = this.onTransitionEnd.bind(this);

    // this.addEventListeners();
  }

  addEventListeners() {
    this.showButtonEl.addEventListener('click', this.openSideNav);
    this.closeButtonEl.addEventListener('click', this.closeSideNav);
    this.sideNavEl.addEventListener('click', this.blockClicks);
    this.sideNavContainerEl.addEventListener('click', this.closeSideNav);
  }

  blockClicks(evt) {
    evt.stopPropagation();
  }

  onTransitionEnd(evt) {
    this.sideNavContainerEl.classList.remove('side-nav-animatable');
    this.sideNavContainerEl.removeEventListener('transitionend', this.onTransitionEnd);
  }

  openSideNav() {
    this.sideNavContainerEl.classList.add('side-nav-animatable');
    this.sideNavContainerEl.classList.add('side-nav-visible');
    this.sideNavContainerEl.addEventListener('transitionend', this.onTransitionEnd);
  }

  closeSideNav() {
    this.sideNavContainerEl.classList.add('side-nav-animatable');
    this.sideNavContainerEl.classList.remove('side-nav-visible');
    this.sideNavContainerEl.addEventListener('transitionend', this.onTransitionEnd);
  }
}

new SideNav();

function separatorNum(val) {
  if (typeof val === 'undefined' || val === null || val === 'null') {
    return null
  }
  val = parseFloat(val)
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

function search(nameKey, myArray) {
  for (var i = 0; i < myArray.length; i++) {
    if (myArray[i].name === nameKey) {
      return myArray[i];
    }
  }
}

function getUniqueFeatures(array, comparatorProperty) {
  var existingFeatureKeys = {};
  // Because features come from tiled vector data, feature geometries may be split
  // or duplicated across tile boundaries and, as a result, features may appear
  // multiple times in query results.
  var uniqueFeatures = array.filter(function (el) {
    if (existingFeatureKeys[el.properties[comparatorProperty]]) {
      return false;
    } else {
      existingFeatureKeys[el.properties[comparatorProperty]] = true;
      return true;
    }
  });

  return uniqueFeatures;
}

$('#kbliID').on('keyup', function () {
  var query = $(this).val();
  var resHTML = ''
  if (query != '' && query.length == 5) {
    $.ajax({
      url: `http://103.214.112.123:3000/area/${query}`,
      method: 'get',
      contentType: false,
      processData: false,
      cache: false,
      beforeSend: function () {

      },
      success: function (res) {
        const dtResp = JSON.parse(res)
        if (dtResp.features != null) {
          const prop = dtResp.features[0].properties
          resHTML += `<div class="form-group">
                  <label for="selKegiatanRuang">Pilih Kegiatan Ruang</label>
                  <select class="form-control" id="selKegiatanRuang">`
          for (var i in prop) {
            resHTML += `<option value="${prop[i]['Lokasi Usaha']}">${prop[i]['Lokasi Usaha']}</option>`
          }
          resHTML += `</select></div>`

          $('#firstKBLIID').html(resHTML)
          $('#secondKBLIID').show()
        }
      },
      error: function (error) {
        console.log(error)
      }
    })
  }
});

$('._kegiatan').on('keyup', function () {
  var query = $(this).val();
  var zonasi = saveSubZona
  var resHTML = ''
  if (query != '' && query.length >= 3) {
    $.ajax({
      url: `http://103.214.112.123:3000/kblia/${zonasi}/${query}`,
      method: 'get',
      dataType: 'json',
      beforeSend: function () {

      },
      success: function (res) {
        console.log(res)
        if (res.features != null) {
          const prop = res.features[0].properties
          resHTML += `<ul class="list-unstyled cont-sel-kegiatan mt-1 mb-0">`
          for (var i in prop) {
            resHTML += `<li class="selKegiatan" data-sektor="${prop[i].Sektor}">${prop[i].Kegiatan}</li>`
          }
          resHTML += `</ul>`

          $('#kegiatanList').fadeIn();
          $('#kegiatanList').html(resHTML);
        }else {
          $('#kegiatanList').fadeOut();
          $('#kegiatanList').html('');
        }
      },
      error: function (error) {
        $('#kegiatanList').html(`<ul class="list-unstyled cont-sel-kegiatan mt-1 mb-0"><li class="selKegiatan">Data tidak ditemukan</li></ul>`);
      }
    })
  }
});

$('#_sektor').on('change', function () {
  getDataKBLI()
})
$('#_kegiatanruang').on('change', function () {
  getDataKBLI()
})
$('#_valmodal').on('change', function () {
  getDataKBLI()
})

$(document).on('click', '.selKegiatan', function () {
  getDataKBLI()

  $('#_kegiatan').val($(this).text());
  $('#_sektor').val($(this).data('sektor'));
  var zonasi = $('.subZonaVal').text()
  var sektor = $('._sektor').val()
  var resHTML = ''
  $.ajax({
    url: `http://103.214.112.123:3000/kblib/${zonasi}/${$(this).text()}/${sektor}`,
    method: 'get',
    dataType: 'json',
    beforeSend: function () {

    },
    success: function (res) {
      if (res.features != null) {
        const prop = res.features[0].properties
        for (var i in prop) {
          resHTML += `<option value="${prop[i]['Lokasi Usaha']}">${prop[i]['Lokasi Usaha']}</option>`
        }

        $('#_kegiatanruang').html(resHTML);
      }
    },
    error: function (error) {
      console.log(error)
    }
  })

  $('#kegiatanList').fadeOut();
});

$('#selSkala').on('change', function () {
  if (map.getLayer('kbliid_fill')) {
    map.removeLayer('kbliid_fill')
  }
  if (map.getLayer('kbliid_dot')) {
    map.removeLayer('kbliid_dot')
  }
  if (map.getSource('kbliid')) {
    map.removeSource('kbliid')
  }

  var query = $(this).val();
  var id = $('#kbliID').val()
  var kegiatan = $('#selKegiatanRuang').val()
  if (query != '' && id && kegiatan) {
    $.ajax({
      url: `http://103.214.112.123:3000/area/${actQ.inputString}/${id}/${kegiatan}/${query}`,
      method: 'get',
      contentType: false,
      processData: false,
      cache: false,
      beforeSend: function () {

      },
      success: function (res) {
        const dtResp = JSON.parse(res)
        if (dtResp.features != null) {
          map.addSource('kbliid', {
            'type': 'geojson',
            'data': dtResp
          });

          map.addLayer({
            'id': 'kbliid_fill',
            'type': 'fill',
            'source': 'kbliid',
            'paint': {
              'fill-color': [
                'case',
                ['==', ['to-string', ['get', 'status']], 'DIIZINKAN'],
                'green',
                ['==', ['to-string', ['get', 'status']], 'TERBATAS'],
                'yellow',
                ['==', ['to-string', ['get', 'status']], 'TERBATAS BERSYARAT'],
                'orange',
                '#000000'
              ]
            },
            'fill-opacity': 0.7,
            'fill-outline-color': 'red',
            'layout': {
              'visibility': 'visible'
            }
          })
        }
      },
      error: function (error) {
        console.log(error)
      }
    })
  }
})

var delay = (function () {
  var timer = 0;
  return function (callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  }
})()


// $("#showKBLIONe").on('click', function () {
//   $(".container-kblione").slideDown("slow", function () {});
// });
// $(".close-kblione").on('click', function () {
//   $(".container-kblione").slideUp("slow", function () {});
// });