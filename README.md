# investasi-ptsp

<h1>Aplikasi untuk investasi daerah di DKI Jakarta<h1>

## Download and Install

Let's go!

```
$ git clone https://gitlab.com/hidden-ptsp/investasi-ptsp.git
$ cd investasi-ptsp
$ npm install
```

## Development

Run development page on **localhost:8080**

```
$ npm run dev
```

## Build

Build for production.

```
$ npm run build
```

## Deploy

Deploy to `gh-pages` branch on GitHub.

```
$ npm run deploy
```

## License

MIT